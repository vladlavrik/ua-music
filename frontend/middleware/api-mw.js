export const CALL_API = 'CALL_API';

export default store => next => action => {

    if (action.type !== CALL_API) {
        return next(action);
    }

    let {
        url,
        method = 'GET',
        param,
        actions,
        actionsParam = {}
    } = action;

    // let body;
    // if (param && (method === 'POST' || method === 'PUT')) {
    //     param = JS
    // }
    //
    // if (param && (method === 'POST' || method === 'PUT')) {
    //     param = JSON.stringify(param);
    // }
    //

    let [loadingAction, loadedAction, loadFailureAction] = actions;


    fetch(url)
        .then(response => {
            let contentType = response.headers.get('Content-Type').toLowerCase();
            let isJson = contentType.includes('application/json');

            if (isJson) {
                return response.json();
            }
            if (response.ok) {
                return response.text();
            }
            return { error: 'unknown' }

        }, error => ({ error: 'network' }))
        .then(data => {
            let error = data.error;
            next(action);

            if (error) {
                store.dispatch({type: loadFailureAction, error, ...actionsParam });
            } else {
                store.dispatch({type: loadedAction, data, ...actionsParam });
            }
        });

    store.dispatch({ type: loadingAction, ...actionsParam });
}