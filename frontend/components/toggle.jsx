import React, { Component } from 'react'

export default class Toggle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checked
        }
    }

    render() {
        let checked = this.state.checked;
        return (
            <div
                className={"toggle " + this.props.className + (checked ? ' is-checked' : '' )}
                onClick={::this.toggle}
            />
        );
    }

    toggle() {
        this.checked = !this.checked;
    }

    get checked() {
        return this.state.checked;
    }
    set checked(checked) {
        this.setState({checked});
    }

}

Toggle.defaultProps = {
    className: '',
    checked: false
};

Toggle.propTypes = {
    className: React.PropTypes.string,
    checked: React.PropTypes.bool,
};