import React, { Component } from 'react'

export default class ListsItem extends Component {
    render() {
        let {id, name, tracksCount} = this.props;
        let isLiked = id === 0;

        return (
            <div className={"lists-item" + (isLiked ? ' is-liked' : '')}>
                <div className="lists-item__area">
                    <button
                        className="lists-item__play"
                        onClick={::this.requirePlay}
                    />
                    <button
                        className="lists-item__details"
                        onClick={::this.requireLoad}
                    />
                </div>
                <p className="lists-item__name">{isLiked ? 'Улюблені' : name}</p>
                <p className="lists-item__info">({tracksCount})</p>
            </div>
        );
    }

    requirePlay(id) {
        this.props.onRequirePlay(this.props.id);
    }

    requireLoad(id) {
        this.props.onRequireLoad(this.props.id);
    }
}

ListsItem.propTypes = {
    id: React.PropTypes.number.isRequired,
    name: React.PropTypes.string,
    tracksCount: React.PropTypes.number.isRequired,
    onRequirePlay: React.PropTypes.func.isRequired,
    onRequireLoad: React.PropTypes.func.isRequired,
};