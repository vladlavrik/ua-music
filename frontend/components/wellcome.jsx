import React, { Component } from 'react'

export default class Welcome extends Component {
    render() {
        return (
            <section className="welcome">
                <div className="welcome__logo"></div>
                {this.props.login && <p className="welcome__login">{this.props.login.toUpperCase()}</p>}
                {!this.props.login && <buttom className="welcome__login">УВІЙТИ</buttom>}
                <h3 className="welcome__header">Велика збірка музики</h3>
                <h4 className="welcome__description">Велика збірка музики, виключно українських виконавців</h4>
            </section>
        );
    }
}


Welcome.propTypes = {
    login: React.PropTypes.string
};