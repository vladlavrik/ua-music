import React, {Component} from 'react'

export default class DropDown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: false
        }
    }

    render() {
        let expanded = this.state.expanded;

        let targetProp = 'data-dd-target';
        let contentProp = 'data-dd-content';
        let activeClassNameProp = 'data-dd-active-class-name';

        let targetNode;
        let contentNode;

        React.Children.forEach(this.props.children, child => {
            let props = child.props;
            let className = props.className || '';

            if (props[targetProp]) {
                let activeClassName = props[activeClassNameProp];

                targetNode = React.cloneElement(child, {
                    [activeClassNameProp]: null,
                    [targetProp]: null,
                    onClick: ::this.toggleExpand,
                    className: className + ' drop-down__target ' + (expanded && activeClassName ? activeClassName : '')
                });
            } else if (props[contentProp] && expanded) {
                contentNode = React.cloneElement(child, {
                    [contentProp]: null,
                    className: className + ' drop-down__content'
                });
            }
        });

        return (
            <div className={"drop-down " + this.props.className}>
                {targetNode}
                {contentNode}
            </div>
        );
    }

    toggleExpand() {
        this.setState({expanded: !this.state.expanded});
    }
}

DropDown.defaultProps = {
    className: ''
};

DropDown.PropTypes = {
    className: React.PropTypes.string
};