import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import DropDown from './drop-down'

export default class Nav extends Component {
    render() {
        return (
            <nav className="nav">
                <ul className="nav__list">
                    <li className="nav__item">
                        <Link className="nav__link" activeClassName="is-active" to='/popular'>
                            Популярне
                        </Link>
                    </li>
                    <li className="nav__item">
                        <Link className="nav__link" activeClassName="is-active" to='/lists'>
                            Мої списки
                        </Link>
                    </li>
                    <li className="nav__item">
                        <Link className="nav__link" activeClassName="is-active" to="/artists">
                            Виконавці
                        </Link>
                    </li>
                    <li className="nav__item">
                        <Link className="nav__link" activeClassName="is-active" to="/genres">
                            Жанри
                        </Link>
                    </li>
                    <li className="nav__item">
                        <Link className="nav__link" activeClassName="is-active" to="/epochs">
                            Епохи
                        </Link>
                    </li>
                    <li className="nav__item">
                        <DropDown className="dd-cln">
                            <p className="nav__more"
                                data-dd-target
                                data-dd-active-class-name="is-active">
                                Ще
                            </p>
                            <ul className="nav__more-list" data-dd-content>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/settings/interface">
                                        Налаштування інтерфейсу
                                    </Link>
                                </li>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/settings/account">
                                        Налаштування аккаунту
                                    </Link>
                                </li>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/settings/password">
                                        Змінити пароль
                                    </Link>
                                </li>
                                <li className="nav__more-separator"/>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/about">
                                        Про сервіс
                                    </Link>
                                </li>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/owners">
                                        Для володарів прав
                                    </Link>
                                </li>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" to="/feedback">
                                        Написати розробникам
                                    </Link>
                                </li>
                                <li className="nav__more-separator"/>
                                <li className="nav__more-item">
                                    <Link className="nav__more-link" activeClassName="is-active" href="/logout">
                                        Вийти
                                    </Link>
                                </li>
                            </ul>
                        </DropDown>
                    </li>
                </ul>
                <input
                    defaultValue={this.props.searchQuery}
                    onInput={::this.search}
                />
                <button className="nav__search"/>
            </nav>
        );
    }

    search({target}) {
        let value = target.value;
        console.log(value);
    }
}

Nav.propTypes = {
    searchQuery: PropTypes.string,
    onSearch: PropTypes.func.isRequired,
};