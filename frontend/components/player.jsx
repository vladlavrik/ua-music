import React, {Component, PropTypes} from 'react'
import Range from './range'
import DropDown from './drop-down'
import Toggle from './toggle'


export default class Player extends Component {

    render() {
        let {list, playedId, paused} = this.props;
        console.log(list, playedId);
        let track = list.find(track => track.id === playedId);
        let cover = track.cover_url
            ? require('../../resources/covers/' + track.cover_url)
            : null;

        return (
            <section className="player">
                {cover && (
                    <img className="player__cover" src={ cover }/>
                )}

                <div className="player__info">
                    <p className="player__info-artist">{track.artist}</p>
                    <p className="player__info-title">{track.title}</p>
                </div>

                <div className="player__nav">
                    <button className={"player__nav-prev"}/>
                    <button className={"player__nav-play" + (paused ? ' is-paused' : '')}/>
                    <button className={"player__nav-next"}/>
                </div>

                <div className="player__controls">
                    <div className="player__control">
                        <div className="player__volume-wrapper">
                            <Range
                                className="player__volume-range"
                                min={0}
                                max={1}
                                step={0.01}
                                value={0.25}/>
                        </div>
                        <button className="player__control-volume"/>
                    </div>
                    <div className="player__control">
                        <button className="player__control-list"/>
                    </div>
                    <DropDown className="player__control">
                        <button
                            className="player__control-more"
                            data-dd-target
                            data-dd-active-class-name="is-active">

                        </button>
                        <ul className="player__more-list" data-dd-content>
                            <li className="player__more-item option-loop">
                                <p className="player__more-name">Повтор треку</p>
                                <Toggle className="player__more-toggle"/>
                            </li>
                            <li className="player__more-item option-shuffle">
                                <p className="player__more-name">Перемiшати</p>
                                <Toggle className="player__more-toggle" checked/>
                            </li>
                            <li className="player__more-item option-equalizer">
                                <p className="player__more-name">Еквалайзер</p>
                                <button className="player__more-settings"/>
                                <Toggle className="player__more-toggle" checked/>
                            </li>
                            <li className="player__more-item option-visualization">
                                <p className="player__more-name">Візуалізація</p>
                                <button className="player__more-settings"/>
                                <Toggle className="player__more-toggle"/>
                            </li>
                        </ul>
                    </DropDown>

                </div>
                <div className="player__progress"/>
            </section>
        )
    }
}

Player.propTypes = {
    list: PropTypes.arrayOf(
        React.PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            artist: PropTypes.string.isRequired,
            duration: PropTypes.number.isRequired,
        }).isRequired
    ).isRequired,
    playedId: PropTypes.number,
    paused: PropTypes.bool
};