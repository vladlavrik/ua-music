import React, {Component, PropTypes} from 'react'

export default class Playlist extends Component {
    render() {

        let {playedId, list, loading} = this.props;

        if (loading) {
            return <p>loading</p>
        }

        return (
            <ul className="playlist">
                { list.map((track, index) => (
                    <li key={index} className={"playlist__item" + (track.id === playedId ? ' is-plays' : '' ) }>
                        <div className="playlist__prefix">
                            <p className="playlist__num">{index + 1}</p>
                            <div className="playlist__plays"></div>
                            <div className="playlist__play"></div>
                            <div className="playlist__pause"></div>
                        </div>
                        <p className="playlist__artist">{ track.artist }</p>
                        <p className="playlist__title">- { track.title }</p>
                        <div className="playlist__time">{ track.duration }</div>
                    </li>
                ))}
            </ul>
        );
    }
}


Playlist.propTypes = {
    list: PropTypes.arrayOf(
        React.PropTypes.shape({
            id: PropTypes.number.isRequired,
            title: PropTypes.string.isRequired,
            artist: PropTypes.string.isRequired,
            duration: PropTypes.number.isRequired,
        }).isRequired
    ).isRequired,
    playedId: PropTypes.number,
    onPlay: PropTypes.func.isRequired
};