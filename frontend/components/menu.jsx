import React, { Component } from 'react'

export default class Menu extends Component {
    render() {
        return (
            <section className="menu">
                <button className="menu-button" onClick={::this.toggleShow}/>
                <menu className="menu-list" hidden={ !this.props.shown }>
                    <li className="menu-item"><a href="">Налаштування інтерфейсу</a></li>
                    <li className="menu-item"><a href="">Налаштування аккаунту</a></li>
                    <li className="menu-item"><a href="">Змінити пароль</a></li>
                    <li className="menu-separator"/>
                    <li className="menu-item"><a href="">Про сервіс</a></li>
                    <li className="menu-item"><a href="">Для володарів прав</a></li>
                    <li className="menu-item"><a href="">Написати розробникам</a></li>
                    <li className="menu-separator"/>
                    <li className="menu-item"><a href="">Вийти</a></li>
                </menu>
            </section>
        );
    }

    toggleShow() {
        if (this.props.shown) {
            this.props.onHide();
        } else {
            this.props.onShow();
        }
    }
}
Menu.propTypes = {
    shown: React.PropTypes.bool.isRequired,
    onShow: React.PropTypes.func.isRequired,
    onHide: React.PropTypes.func.isRequired,
};