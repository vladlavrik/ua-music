import React, { Component } from 'react'
import ReactDom from 'react-dom'

//TODO props: point, controlled, disabled, direction, separator for 0, only left mouse key

export default class Range extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    }

    render() {
        let {value} = this.state;
        let percentage = this.getPercentage(value);

        return (
            <section className={ "range " + this.props.className }
                onMouseDown={::this.startChange}
                onTouchStart={::this.startChange}>
                <div className="range__way"></div>
                <div className="range__completed" style={ {width: percentage + '%'} }></div>
                <div className="range__wrapper">
                    <div className="range__point" style={ {left: percentage + '%'} }></div>
                </div>
            </section>
        );
    }

    componentWillMount() {

    }

    componentWillUnmount() {
    }

    startChange(event) {
        event.preventDefault();
        let touches = event.type === 'touchstart' ? event.touches[0] : event;
        this.handleChange(touches.clientX, touches.clientY);

        this.__stepChange = ::this.stepChange;
        this.__stopChange = ::this.stopChange;
        document.addEventListener('mousemove', this.__stepChange);
        document.addEventListener('touchmove', this.__stepChange);
        document.addEventListener('mouseup', this.__stopChange);
        document.addEventListener('touchend', this.__stopChange);
    }

    stepChange (event) {
        event.preventDefault();
        let touches = event.type === 'touchmove' ? event.touches[0] : event;
        this.handleChange(touches.clientX, touches.clientY);
    }

    stopChange (event) {
        event.preventDefault();
        let touches = event.type === 'touchend' ? event.changedTouches[0] : event;
        this.handleChange(touches.clientX, touches.clientY);

        document.removeEventListener('mousemove', this.__stepChange);
        document.removeEventListener('touchmove', this.__stepChange);
        document.removeEventListener('mouseup', this.__stopChange);
        document.removeEventListener('touchend', this.__stopChange);
        delete this.__stepChange;
        delete this.__stopChange;
    }


    handleChange(posX, posY) {
        console.log(posX);
        let value = this.calculateValue(posX, posY);
        if (value !== this.state.value) {
            this.setState({value});
        }
    }


    calculateValue(posX, posY) {
        let elem = ReactDom.findDOMNode(this);
        let {min, max, step} = this.props;
        let length = max - min;
        let {left: elemX, top: elemY} = elem.getBoundingClientRect();
        let elemWidth = elem.clientWidth;
        let elemHeight = elem.clientHeight;
        let horizontal = this.props.direction === 'horizontal';

        let percent = horizontal
            ? +((posX - elemX) / elemWidth)
            : -((posY - elemY) / elemHeight - 1);

        let absValue = percent * length;
        let value = absValue + min;

        let residueMax = Math.abs(max - value);
        let residueMin = Math.abs(min - value);
        let residue = Math.min(residueMin, residueMax);

        if (step && step / 2 > residue) {
            return absValue > length / 2 ? max : min;
        }

        value = step ? Math.round(value / step) * step : value;
        value = Math.min(value, max);
        value = Math.max(value, min);

        return value;
    }


    getPercentage(value) {
        let {min, max} = this.props;
        let length = max - min;
        return Math.abs(value - min) / length * 100;
    }

}

Range.defaultProps = {
    direction: 'horizontal',
    disabled: false,
    point: false,
    min: 0,
    max: 10,
    step: 1,
    value: 0,
    controlled: false
};
Range.propTypes = {
    direction: React.PropTypes.oneOf(['horizontal', 'vertical']),
    disabled: React.PropTypes.bool,
    point: React.PropTypes.bool,
    min: React.PropTypes.number,
    max: React.PropTypes.number,
    step: React.PropTypes.number,
    value: React.PropTypes.number,
    controlled: React.PropTypes.bool
};