import React, { Component } from 'react'

export default class NotFound extends Component {
    render() {
        return (
            <section className="not-found">
                NotFound
            </section>
        )
    }
}