import React, { Component } from 'react'
import ListsItem from '../components/lists-item';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {loadLists} from '../actions/lists-actions';
import {loadPlaylist} from '../actions/playlist-actions';

class Lists extends Component {
    render() {
        let {loading, list} = this.props;

        if (loading) {
            return <h2>LOADING</h2>;
        }

        return (
            <section className="lists" ref="root">
                { list.map((item, index) => (
                    <ListsItem
                        key={index}
                        id={item.id}
                        name={item.name}
                        tracksCount={item.tracksCount}
                        onRequirePlay={::this.requirePlay}
                        onRequireLoad={::this.requireLoad}
                    />
                )) }

                {/*fix flex-box table*/}
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
                <div className="lists__dummy-item"/>
            </section>
        );
    }

    requirePlay(id) {

        this.props.loadPlaylist('list', id, true);
    }

    requireLoad(id) {
        this.props.loadPlaylist('list', id);
    }

    componentDidMount() {
        this.props.loadLists();
    }
}

Lists.propTypes = {
    loading: React.PropTypes.bool.isRequired,
    list: React.PropTypes.arrayOf(
        React.PropTypes.shape({
            id: React.PropTypes.number.isRequired,
            name: React.PropTypes.string,
            tracksCount: React.PropTypes.number.isRequired,
        })
    ).isRequired
};

export default connect(
    state => ({
        loading: state.lists.loading,
        list: state.lists.list,
        listDetails: state.lists.listDetails
    }),
    dispatch => bindActionCreators({loadLists, loadPlaylist}, dispatch)
)(Lists);
