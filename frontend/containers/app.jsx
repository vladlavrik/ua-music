import React, { Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import WellCome from '../components/wellcome';
import Player from '../components/player';
import Nav from '../components/nav';
import Playlist from '../components/playlist';
import {loadProfile, setLang} from '../actions/app-actions';
import {loadPlaylist, setPlaylist} from '../actions/playlist-actions';
import {setPlayerList} from '../actions/player-actions';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {ready, login, player, playlist, children} = this.props;

        if(!ready) {
            return <p>LOADING</p>;
        }

        let showPlaylist = playlist.sourceType !== null;
        let showPlayer = !!player.list.length;

        return (
            <div className="app">
                <header className="header">
                    <div className="header__content">
                        {showPlayer && (
                            <Player
                                list={player.list}
                                playedId={player.playedId}
                                paused={player.paused}
                            />
                        )}

                        {!showPlayer && (
                            <WellCome login={login}/>
                        )}

                        <Nav
                            searchQuery={ playlist.search }
                            onSearch={::this.playlistSearch}
                        />
                    </div>
                </header>

                <div className={"main" + (showPlaylist ? ' is-half' : '')}>
                    {children}
                </div>

                {showPlaylist && (
                    <Playlist
                        list={playlist.list}
                        playedId={playlist.playedId}
                        paused={playlist.paused}
                        onPlay={::this.playlistPlay}
                    />
                )}
            </div>
        )
    }

    playlistSearch(searchQuery) {
        console.log(!!this, 'search', searchQuery);
    }

    playlistPlay(trackId) {
        console.log(!!this, 'play', trackId);
    }

    componentDidMount() {
        this.props.loadProfile();
    }
}

App.propTypes = {
    ready: PropTypes.bool.isRequired,
    login: PropTypes.string,
    lang: PropTypes.string,
    loadProfile: PropTypes.func.isRequired,
    setLang: PropTypes.func.isRequired,
    player: PropTypes.shape({
        playedId: PropTypes.number,
        paused: PropTypes.bool,
    }).isRequired,
    playlist: PropTypes.shape({
        sourceType: PropTypes.oneOf([null,'list', 'artist', 'album', 'global']),
        sourceId: PropTypes.number,
        playedId: PropTypes.number,
        paused: PropTypes.bool,
        searchQuery: PropTypes.string,
        list: PropTypes.array.isRequired,
    }).isRequired,
};

export default connect(
    state => ({
        ready: state.app.ready,
        login: state.app.login,
        lang: state.app.lang,
        player: state.player,
        playlist: state.playlist
    }),
    dispatch => bindActionCreators({
        loadProfile,
        setLang,
        loadPlaylist,
        setPlaylist,
        setPlayerList
    }, dispatch)
)(App);
