import {createStore, combineReducers, applyMiddleware} from 'redux';
import apiMiddleware from './middleware/api-mw';
import * as reducers from './reducers';

const reducer = combineReducers(reducers);
const devTools = process.env.NODE_ENV === 'development'
    && window.__REDUX_DEVTOOLS_EXTENSION__
    && window.__REDUX_DEVTOOLS_EXTENSION__();

const createStoreWithMiddleware = applyMiddleware(apiMiddleware)(createStore);

export default createStoreWithMiddleware(reducer, {
    app: {
        ready: false,
        login: null,
        lang: document.querySelector('html').lang,
        menuShown: false
    },
    player: {
        paused: false,
        playedId: null,
        list: []
    },
    playlist: {
        sourceType: null,
        sourceId: null,
        playedId: null,
        paused: false,
        searchQuery: null,
        list: [],
    },
    lists: {
        list: [],
        loading: false
    }
}, devTools);