import {
    LISTS_LOADING,
    LISTS_LOADED,
    LISTS_LOAD_FAILURE,
} from '../actions/lists-actions';


export default function (state = {}, action) {
    switch (action.type) {
        case LISTS_LOADING:
            return {
                ...state,
                loading: true
            };

        case LISTS_LOADED:
            return {
                ...state,
                list: action.data.lists,
                loading: false
            };

        case LISTS_LOAD_FAILURE:
            return {
                ...state,
                loading: false
            };

        default:
            return state;
    }

}