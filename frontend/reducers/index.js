import app from './app-reducer';
import player from './player-reducer';
import playlist from './playlist-reducer';
import lists from './lists-reducer';

export {app, player, playlist, lists};