import {SET_PLAYER_LIST} from '../actions/player-actions';
import {PLAYLIST_LOADED} from '../actions/playlist-actions';

export default function (state = {}, action) {
    switch (action.type) {
        case PLAYLIST_LOADED:
            if (!action.autoPlay) {
                return state;
            }

            let list = action.data.tracks;
            let playedId = action.playId === null
                ? list[0].id
                : action.playId;
            return {
                ...state,
                paused: false,
                playedId,
                list
            };

        case SET_PLAYER_LIST:
            return {
                ...state,
            };

        default:
            return state;
    }
}