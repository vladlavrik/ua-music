import {
    PLAYLIST_LOADING,
    PLAYLIST_LOADED,
    PLAYLIST_LOAD_FAILURE
} from '../actions/playlist-actions';

export default function (state = {}, action) {
    switch (action.type) {
        case PLAYLIST_LOADING:
            return {
                ...state,
                loading: true
            };

        case PLAYLIST_LOADED:
            let list = action.data.tracks;
            let playedId = null;

            if (action.autoPlay) {
                playedId = action.playId === null
                    ? list[0].id
                    : action.playId;
            }

            return {
                ...state,
                loading: false,
                sourceType: action.sourceType,
                sourceId: action.sourceId,
                playedId,
                list
            };

        case PLAYLIST_LOAD_FAILURE:
            return {
                ...state,
                loading: false
            };

        default:
            return state;
    }

}