import {
    PROFILE_LOADING, PROFILE_LOADED, PROFILE_LOAD_FAILURE, SET_LANG
} from '../actions/app-actions';

export default function (state = {}, action) {
    switch (action.type) {
        case PROFILE_LOADING:
            return state;

        case PROFILE_LOADED:
            return {
                ...state,
                ready: true,
                lang: action.data.lang,
                login: action.data.login
            };

        case PROFILE_LOAD_FAILURE:
            if (action.error === 'unauthorized') {
                return {
                    ...state,
                    ready: true,
                    lang: 'uk' // get from browser or default
                };
            }
            return state;

        case SET_LANG:
            return {...state, lang: action.lang};

        default:
            return state;
    }

}