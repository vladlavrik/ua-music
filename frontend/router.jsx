import React from 'react';
import {Router, Route, IndexRedirect, browserHistory} from 'react-router';
import App from './containers/app'
import Lists from './containers/lists'
import Artists from './containers/artists'
import Albums from './containers/albums'
import Genres from './containers/genres'
import Epochs from './containers/epochs'
import NotFound from './components/not-found'

export default (
    <Router history={browserHistory}>
        <Route path='/' component={App}>
            <IndexRedirect to='lists'/>
            <Route path='lists' component={Lists}/>
            <Route path='artists' component={Artists}/>
            <Route path='albums' component={Albums}/>
            <Route path='genres' component={Genres}/>
            <Route path='epochs' component={Epochs}/>
            <Route path='*' component={NotFound}/>
        </Route>
    </Router>
);
