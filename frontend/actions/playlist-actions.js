import { CALL_API } from '../middleware/api-mw'

export const PLAYLIST_LOADING = 'PLAYLIST_LOADING';
export const PLAYLIST_LOADED = 'PLAYLIST_LOADED';
export const PLAYLIST_LOAD_FAILURE = 'PLAYLIST_LOAD_FAILURE';
export const PLAYLIST_SET = 'PLAYLIST_SET';


/**
 * @param {('list'|'artist'|'album'|'global')} sourceType
 * @param {number} sourceId
 * @param {boolean} [autoPlay=false]
 * @param {number|null} [playId=null]
 * @param {string|null} [searchQuery=null]
 * @param {boolean} [globalSearch=false]
 */
export const loadPlaylist = (sourceType, sourceId, autoPlay=false, playId=null, searchQuery=null, globalSearch=false) => ({
    type: CALL_API,
    url: `/api/playlist/${sourceType}/${sourceId}`,
    param: {
        searchQuery, globalSearch
    },
    actions: [PLAYLIST_LOADING, PLAYLIST_LOADED, PLAYLIST_LOAD_FAILURE],
    actionsParam: {
        sourceType, sourceId, autoPlay, playId
    }
});


/**
 * @param {Array} list
 * @param {('list'|'artist'|'album'|'global')} sourceType
 * @param {number} sourceId
 * @param {boolean} [autoPlay=false]
 * @param {number} [playId]
 * @param {string} [searchQuery]
 * @param {boolean} [globalSearch=false]
 */
export const setPlaylist = (list, sourceType, sourceId, autoPlay=false, playId, searchQuery, globalSearch=false) => ({
    type: PLAYLIST_SET,
    sourceType,
    sourceId,
    playId,
    searchQuery,
    globalSearch
});