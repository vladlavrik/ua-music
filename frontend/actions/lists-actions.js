import { CALL_API } from '../middleware/api-mw'

export const LISTS_LOADING = 'LISTS_LOADING';
export const LISTS_LOADED = 'LISTS_LOADED';
export const LISTS_LOAD_FAILURE = 'LISTS_LOAD_FAILURE';

export const loadLists = () => ({
    type: CALL_API,
    url: '/api/lists',
    actions: [LISTS_LOADING, LISTS_LOADED, LISTS_LOAD_FAILURE]
});


export const createList = () => ({});


export const renameList = () => ({});
