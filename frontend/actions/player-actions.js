export const SET_PLAYER_LIST = 'SET_PLAYER_LIST';

export const setPlayerList = (list, playId) => ({
    type: SET_PLAYER_LIST,
    list,
    playId
});