import { CALL_API } from '../middleware/api-mw'

export const PROFILE_LOADING = 'PROFILE_LOADING';
export const PROFILE_LOADED = 'PROFILE_LOADED';
export const PROFILE_LOAD_FAILURE = 'PROFILE_LOAD_FAILURE';
export const SET_LANG = 'SET_LANG';

export const loadProfile = () => ({
    type: CALL_API,
    url: '/api/user_profile',
    actions: [PROFILE_LOADING, PROFILE_LOADED, PROFILE_LOAD_FAILURE]
});


export const setLang = (lang) => ({
    type: SET_LANG,
    lang
});