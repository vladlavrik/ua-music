const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

let isProd = process.env.NODE_ENV === 'production';

module.exports = {
    context: __dirname + '/frontend',
    entry: {
        bundle: [
            ...(isProd ? [] : [
                'webpack-dev-server/client',
                'webpack/hot/dev-server'
            ]),
            './index'
        ]
    },
    output: {
        path: __dirname + '/public',
        filename: isProd ? '[chunkhash].js' : '[name].js',
        publicPath: '/'
    },

    watch: !isProd,
    watchOptions: {
        aggregateTimeout: 100
    },

    devtool: isProd ? 'source-map' : 'inline-source-map',

    module: {
        loaders: [{
            test: /\.jsx?$/,
            include: __dirname + '/frontend',
            loader: 'babel',
            query: {
                presets: ['react', 'es2015'],
                plugins: [
                    'transform-object-rest-spread',
                    'transform-class-properties',
                    'transform-function-bind'
                ],
                cacheDirectory: __dirname + '/tmp'
            }
        }, {
            test: /\.styl$/,
            loader: ExtractTextPlugin.extract('style', 'css!stylus?resolve url')
        }, {
            test: /\.(svg|png|jpeg|jpg|ttf|ico)$/,
            loader: 'file',
            query: {
                name: isProd ? '[hash:6].[ext]' : '[path][name].[ext]?[hash:6]',
            }

        }],

        noParse: [] // TODO node_modules libs
    },


    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(isProd ? 'production' : 'development')
            }
        }),
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './index.html',
            favicon: 'style/favicon.ico'
        }),
        new ExtractTextPlugin('[name].css', {
            disable: !isProd
        }),
        ...(isProd ? [] : [
            new webpack.HotModuleReplacementPlugin()
        ])
    ],


    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.jsx', '.styl'],
    },

    devServer: {
        host: '0.0.0.0',
        historyApiFallback: true,
        hot: true,
        proxy: {
            '/api': {
                target: 'http://localhost:8081',
                secure: false
            }
        }
    }
};